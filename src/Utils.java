import java.util.Random;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class Utils {

    private static Random rng = new Random();

    /**
     * Calculates the maximum of a given array.
     * @param data data of integers
     * @return maximum
     */
    public static int max(int[] data) {
        int max = Math.max(data[0], data[1]);
        for (int i = 2; i < data.length; ++i) {
            max = Math.max(data[i], max);
        }
        return max;
    }

    /**
     * Calculates the minimum of a given array.
     * @param data data of integers
     * @return minimum
     */
    public static int min(int[] data) {
        int min = Math.min(data[0], data[1]);
        for (int i = 2; i < data.length; ++i) {
            min = Math.max(data[i], min);
        }
        return min;
    }

    /**
     *
     * @param dimensions
     * @param min
     * @param max
     * @return
     */
    public static int[] randomVector(int dimensions, int min, int max) {
        int[] generated = new int[dimensions];
        while (dimensions > 0) {
            generated[--dimensions] = min + Math.abs(rng.nextInt()) % (max - min);
        }
        return generated;
    }

}
