package algorithm;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a general functionality for an algorithm.
 * @author danci
 * @version 2020.1
 */
public abstract class AbstractAlgorithm {

    /**
     * Data to be sorted. Can have duplicates.
     */
    protected int[] data;

    /**
     * Number of comparisons made so far.
     */
    protected int nComparisons;

    /**
     * Number of reads made so far.
     */
    protected int nReads;

    /**
     * Number of writes made so far.
     */
    protected int nWrites;

    /**
     * Stores indices that are at their correct places at
     * some stage of the algorithm.
     */
    protected Set<Integer> sorted = new HashSet<>();


    /**
     * Initializes an integer array which will be sorted.
     * @param data an array to be sorted
     */
    public AbstractAlgorithm(int[] data) {
        this.data = data;
    }

    /**
     * @return number of comparisons made so far
     */
    public int nComparisons() {
        return nComparisons;
    }

    /**
     * @return number of array reads made so far
     */
    public int nReads() {
        return nReads;
    }

    /**
     * @return number of array writes made so far
     */
    public int nWrites() {
        return nWrites;
    }

    /**
     * Adds an index to a set of sorted indices.
     * @param index index of an element that is at the correct place
     */
    public void registerSortedIndex(int index) {
        sorted.add(index);
    }

    /**
     * Marks all indices sorted.
     */
    protected void registerAll() {
        for (int i = 0; i < data.length; ++i) {
            registerSortedIndex(i);
        }
    }

    /**
     * @return sorted indices
     */
    public Set<Integer> getSorted() {
        return sorted;
    }

    /**
     * Makes a next step in the algorithm.
     */
    public abstract void nextStep();

    /**
     * @return true if the algorithm has no further steps to make, false otherwise
     */
    public abstract boolean hasFinished();

}
