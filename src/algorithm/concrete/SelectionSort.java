package algorithm.concrete;

import algorithm.AbstractAlgorithm;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class SelectionSort extends AbstractAlgorithm {

    /**
     *
     */
    private int sortedCounter;

    /**
     *
     */
    private int innerCounter;

    /**
     *
     */
    private boolean finished;

    /**
     *
     */
    private int minimumFound;

    private int minFoundIndex;

    /**
     *
     * @param data
     */
    public SelectionSort(int[] data) {
        super(data);
        minimumFound = data[0];
        minFoundIndex = 0;
    }

    @Override
    public void nextStep() {
        if (!finished) {
            if (innerCounter < data.length) {
                ++nComparisons;
                ++nReads;
                int retrieved = data[innerCounter];
                if (retrieved < minimumFound) {
                    minimumFound = retrieved;
                    minFoundIndex = innerCounter;
                }
            }
            ++innerCounter;
            if (innerCounter == data.length) {
                nReads += 2;
                nWrites += 2;
                data[minFoundIndex] = data[sortedCounter];
                data[sortedCounter++] = minimumFound;
                registerSortedIndex(sortedCounter - 1);
                innerCounter = sortedCounter + 1;
                minimumFound = data[sortedCounter];
                minFoundIndex = sortedCounter;
                if (sortedCounter == data.length - 1) {
                    registerSortedIndex(sortedCounter);
                    finished = true;
                }
            }
            if (sortedCounter >= data.length) {
                registerSortedIndex(sortedCounter);
                finished = true;
            }
        }
    }

    @Override
    public boolean hasFinished() {
        return finished;
    }

    @Override
    public String toString() {
        return "selection sort";
    }

}
