package algorithm.concrete;

import algorithm.AbstractAlgorithm;
import jdk.jshell.spi.ExecutionControl;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class QuickSort extends AbstractAlgorithm {

    /**
     *
     */
    private boolean finished;

    /**
     *
     */
    private int counter;

    /**
     *
     */
    private List<int[]> steps = new ArrayList<>();

    /**
     *
     * @param data
     */
    public QuickSort(int[] data) {
        super(data);
        run();
    }

    private void run() {

    }

    private void quicksort(int[] data) {
        if (data.length <= 2) {
            return;
        } else {

        }
    }

    @Override
    public void nextStep() {
        throw new IllegalArgumentException();
        // if (!finished) {
        //     if (counter >= steps.size()) {
        //         finished = true;
        //         return;
        //     } else {
        //         data = steps.get(counter++);
        //     }
        // }
    }

    @Override
    public boolean hasFinished() {
        return finished;
    }

    @Override
    public String toString() {
        return "quick sort";
    }

}
