package algorithm.concrete;

import algorithm.AbstractAlgorithm;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class InsertionSort extends AbstractAlgorithm {

    /**
     *
     */
    private int rightBound, rightCounter;

    /**
     *
     */
    private boolean finished;

    /**
     *
     * @param data
     */
    public InsertionSort(int[] data) {
        super(data);
        rightBound = 1;
        rightCounter = 1;
    }

    @Override
    public void nextStep() {
        if (rightBound >= data.length) {
            finished = true;
            registerAll();
        }
        if (!finished) {
            if (rightCounter > 0) {
                int r = data[rightCounter];
                int l = data[rightCounter - 1];
                nReads += 2;
                ++nComparisons;
                if (r < l) {
                    nWrites += 2;
                    data[rightCounter] = l;
                    data[rightCounter - 1] = r;
                } else {
                    rightCounter = ++rightBound;
                    return;
                }
                --rightCounter;
            } else {
                rightCounter = ++rightBound;
            }
        }
    }

    @Override
    public boolean hasFinished() {
        return finished;
    }

    @Override
    public String toString() {
        return "insertion sort";
    }

}
