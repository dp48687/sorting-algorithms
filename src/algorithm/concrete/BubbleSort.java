package algorithm.concrete;

import algorithm.AbstractAlgorithm;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class BubbleSort extends AbstractAlgorithm {

    /**
     *
     */
    private int outerCounter;

    /**
     *
     */
    private int innerCounter;

    /**
     *
     */
    private boolean finished;

    /**
     *
     */
    private boolean swapped;

    /**
     *
     * @param data
     */
    public BubbleSort(int[] data) {
        super(data);
    }

    @Override
    public void nextStep() {
        if (!finished) {
            if (outerCounter < data.length - 1) {
                if (innerCounter < data.length - outerCounter - 1) {
                    nReads += 2;
                    int l = data[innerCounter];
                    int r = data[innerCounter + 1];
                    ++nComparisons;
                    if (l > r) {
                        data[innerCounter] = r;
                        data[innerCounter + 1] = l;
                        nWrites += 2;
                        swapped = true;
                    }
                    ++innerCounter;
                    if (innerCounter >= data.length - outerCounter - 1) {
                        innerCounter = 0;
                        ++outerCounter;
                        registerSortedIndex(data.length - outerCounter);
                        if (!swapped) {
                            finished = true;
                            registerAll();
                            return;
                        }
                        swapped = false;
                        if (outerCounter >= data.length - 1) {
                            finished = true;
                            registerAll();
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean hasFinished() {
        return finished;
    }

    @Override
    public String toString() {
        return "bubble sort";
    }

}
