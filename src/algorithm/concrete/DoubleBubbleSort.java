package algorithm.concrete;

import algorithm.AbstractAlgorithm;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class DoubleBubbleSort extends AbstractAlgorithm {

    /**
     *
     */
    private int sortedCounterL, sortedCounterR;

    /**
     *
     */
    private int innerCounterL, innerCounterR;

    /**
     *
     */
    private boolean finished;

    /**
     *
     */
    private boolean swappedRight, swappedLeft;

    /**
     *
     */
    private boolean turnLeft;

    /**
     *
     * @param data
     */
    public DoubleBubbleSort(int[] data) {
        super(data);
        sortedCounterL = -1;
        sortedCounterR = data.length;
        innerCounterR = 0;
        innerCounterL = data.length - 2;
    }

    @Override
    public void nextStep() {
        if (!finished) {
            if (sortedCounterL >= sortedCounterR) {
                finished = true;
                registerAll();
                return;
            }

            if (turnLeft) {
                if (innerCounterL <= sortedCounterL + 1) {
                    turnLeft = !turnLeft;
                    if (!swappedLeft) {
                        finished = true;
                        registerAll();
                        return;
                    }
                    innerCounterL = sortedCounterR - 1;
                    registerSortedIndex(++sortedCounterL);
                    swappedLeft = false;
                } else {
                    nReads += 2;
                    int r = data[innerCounterL];
                    int l = data[innerCounterL - 1];
                    ++nComparisons;
                    if (r < l) {
                        nWrites += 2;
                        swappedLeft = true;
                        data[innerCounterL] = l;
                        data[innerCounterL - 1] = r;
                    }
                    --innerCounterL;
                }

            } else {
                if (innerCounterR >= sortedCounterR - 1) {
                    turnLeft = !turnLeft;
                    if (!swappedRight) {
                        finished = true;
                        registerAll();
                        return;
                    }
                    innerCounterR = sortedCounterL + 1;
                    registerSortedIndex(--sortedCounterR);
                    swappedRight = false;
                } else {
                    nReads += 2;
                    int l = data[innerCounterR];
                    int r = data[innerCounterR + 1];
                    ++nComparisons;
                    if (l > r) {
                        ++nWrites;
                        swappedRight = true;
                        data[innerCounterR] = r;
                        data[innerCounterR + 1] = l;
                    }
                    ++innerCounterR;
                }
            }
        }
    }

    @Override
    public boolean hasFinished() {
        return finished;
    }

    @Override
    public String toString() {
        return "double bubble sort";
    }

}
