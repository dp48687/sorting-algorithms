package algorithm;

import algorithm.concrete.*;

/**
 * Used to create algorithms.
 * @author danci
 * @version 2020.1
 */
public class AlgorithmFactory {

    /**
     * Instantiates a concrete algorithm for a given ID.
     * @param parameters info used in creation of the algorithm
     * @param dataToSort integers to be sorted
     * @return a created algorithm
     */
    public static AbstractAlgorithm create(String parameters, int[] dataToSort) {
        switch (parameters.toLowerCase().replace(" ", "")) {
            case "bubblesort":
                return new BubbleSort(dataToSort);
            case "selectionsort":
                return new SelectionSort(dataToSort);
            case "doublebubblesort":
                return new DoubleBubbleSort(dataToSort);
            case "insertionsort":
                return new InsertionSort(dataToSort);
            default:
                throw new IllegalArgumentException("Unknown algorithm description: " + parameters);
        }
    }

    /**
     * @return possible algorithm names
     */
    public static String[] getSupportedAlgorithms() {
        return new String[]{
                "bubble sort",
                "double bubble sort",
                "selection sort",
                "insertion sort"
        };
    }

}
