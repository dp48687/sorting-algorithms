import algorithm.AlgorithmFactory;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.util.Set;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class ArrayView extends Canvas {

    /**
     * Used in determining the position of columns.
     */
    private static final int OFFSET_LEFT = 5;
    private static final int OFFSET_RIGHT = 5;
    private static final int OFFSET_UP = 5;
    private static final int OFFSET_DOWN = 5;
    private static final double OFFSET_BETWEEN_RATIO = 0.15;

    /**
     * Used to draw objects on a screen.
     */
    private GraphicsContext gc;

    private HBox buttonRow = new HBox();

    private ArrayController controller;

    /**
     * Initializes a graphics context object.
     */
    public ArrayView(ArrayController controller) {
        gc = getGraphicsContext2D();
        this.controller = controller;
        makeFooter();
    }

    /**
     *
     * @param data
     * @param alreadySorted
     * @param nComparisons
     * @param nReads
     * @param nWrites
     */
    public synchronized void paintArray(int[] data,
                                        Set<Integer> alreadySorted,
                                        int nComparisons,
                                        int nReads,
                                        int nWrites) {
        gc.setFill(Color.WHITE);
        gc.clearRect(0, 0, getWidth(), getHeight());
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.BLUE);
        float widthAvailablePerColumn = (float) ((getWidth() - OFFSET_LEFT - OFFSET_RIGHT) / data.length);
        float height = (float) (getHeight() - OFFSET_DOWN - OFFSET_UP);
        int max = Utils.max(data);
        double OFFSET_BETWEEN = widthAvailablePerColumn * OFFSET_BETWEEN_RATIO;
        for (int i = 0; i < data.length; ++i) {
            float heightCurrent = height * ((float) (data[i]) / max);
            gc.setFill(alreadySorted.contains(i) ? Color.GREEN.brighter().brighter() : Color.GREEN);
            gc.fillRect(
                    OFFSET_LEFT + widthAvailablePerColumn * i + OFFSET_BETWEEN / 2.0,
                    getHeight() - OFFSET_DOWN - heightCurrent,
                    widthAvailablePerColumn - OFFSET_BETWEEN,
                    heightCurrent
            );
        }
        gc.strokeText("Broj elemenata: " + data.length, OFFSET_UP + 10, OFFSET_LEFT + 10);
        gc.strokeText("Broj usporedbi: " + nComparisons, OFFSET_UP + 10, OFFSET_LEFT + 30);
        gc.strokeText("Broj čitanja iz polja: " + nReads, OFFSET_UP + 10, OFFSET_LEFT + 50);
        gc.strokeText("Broj pisanja u polje: " + nWrites, OFFSET_UP + 10, OFFSET_LEFT + 70);
    }

    public void onInitClicked() {
        controller.init();
    }

    public void onNextClicked() {
        controller.next();
    }

    public void onPlayClicked() {
        controller.play();
    }

    public void onStopClicked() {
        controller.stop();
    }

    private Control customize(Control control) {
        control.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        HBox.setHgrow(control, Priority.ALWAYS);
        HBox.setMargin(control, new Insets(1, 5, 1, 5));
        return control;
    }

    @SuppressWarnings("unchecked")
    private void makeFooter() {
        Button init = (Button) customize(new Button("INIT"));
        init.setOnAction(event -> onInitClicked());

        Button next = (Button) customize(new Button("SLJEDEĆI KORAK"));
        next.setOnAction(event -> onNextClicked());

        Button play = (Button) customize(new Button("POKRENI"));
        play.setOnAction(event -> onPlayClicked());

        Button stop = (Button) customize(new Button("STOP"));
        stop.setOnAction(event -> onStopClicked());

        double initialValue = 0.4;
        Slider slider = (Slider) customize(new Slider(0.0, 1.0, initialValue));
        slider.valueProperty().addListener((obs, o, n) -> controller.onSpeedSet(n.doubleValue()));
        controller.onSpeedSet(initialValue);

        int minColumns = Info.N_ELEMENTS_MIN;
        int maxColumns = Info.N_ELEMENTS_MAX;
        int initialNColumns = (minColumns + maxColumns) / 2;
        Slider nColumnsSlider = (Slider) customize(new Slider(minColumns, maxColumns, initialNColumns));
        nColumnsSlider.valueProperty().addListener((obs, o, n) -> controller.onNumberOfColumnsSet(n.intValue()));
        controller.onNumberOfColumnsSet(initialNColumns);

        ChoiceBox<String> algorithmList = (ChoiceBox<String>) customize(new ChoiceBox<>());
        for (String algorithmID : AlgorithmFactory.getSupportedAlgorithms()) {
            algorithmList.getItems().add(algorithmID);
        }
        algorithmList.setValue(algorithmList.getItems().get(0));
        algorithmList.valueProperty().addListener((obs, o, n) -> controller.onAlgorithmChosen(n));
        controller.onAlgorithmChosen(algorithmList.getValue());

        buttonRow.getChildren().add(algorithmList);
        buttonRow.getChildren().add(customize(new Label("Broj stupaca:")));
        buttonRow.getChildren().add(nColumnsSlider);
        buttonRow.getChildren().add(customize(new Label("Brzina:")));
        buttonRow.getChildren().add(slider);
        buttonRow.getChildren().add(init);
        buttonRow.getChildren().add(next);
        buttonRow.getChildren().add(play);
        buttonRow.getChildren().add(stop);

        VBox outerWrapper = new VBox();
        outerWrapper.getChildren().add(buttonRow);
    }

    public void freezeControls() {
        for (Node node : buttonRow.getChildren()) {
            if (node instanceof Button) {
                Button button = (Button) node;
                switch (button.getText().toLowerCase()) {
                    case "play":
                    case "next":
                    case "init":
                        button.setDisable(true);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    public void unfreezeControls() {
        buttonRow.getChildren().forEach(n -> n.setDisable(false));
    }

    public Parent getFooter() {
        return buttonRow;
    }

}
