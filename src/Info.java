/**
 * Holds various data.
 *
 * @author danci
 * @version 2020.1
 */
public class Info {

    /**
     * A window width.
     */
    public static final int WIDTH = 1000;

    /**
     * A window height.
     */
    public static final int HEIGHT = 600;

    public static final int MIN_DATA_VALUE = 1;

    public static final int MAX_DATA_VALUE = 1000;

    public static final int N_ELEMENTS_MIN = 10;

    public static final int N_ELEMENTS_MAX = 400;

    public static final int ANIMATION_DELAY_MINIMUM = 10;

    public static final int ANIMATION_DELAY_MAXIMUM = 100;

    private Info() {

    }

}
