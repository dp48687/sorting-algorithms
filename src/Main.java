import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        ArrayController controller = new ArrayController(new ArrayModel());
        ArrayView canvas = controller.getView();
        canvas.setWidth(Info.WIDTH);
        canvas.setHeight(Info.HEIGHT);
        root.getChildren().add(canvas);
        root.getChildren().add(canvas.getFooter());
        primaryStage.setTitle("Sorting algorithms");
        primaryStage.setResizable(false);
        Scene scene = new Scene(root, Info.WIDTH, Info.HEIGHT + 45);
        primaryStage.setScene(scene);
        controller.onDataChangedOrSet();
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
