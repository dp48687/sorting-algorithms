import algorithm.AbstractAlgorithm;
import algorithm.AlgorithmFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class ArrayModel {

    /**
     *
     */
    private int[] data;

    /**
     *
     */
    private AbstractAlgorithm sortAlgorithm;

    /**
     *
     */
    private List<DataChangedListener> listeners;

    public ArrayModel() {

    }

    public void init(int nElements, String sortAlgorithm) {
        setData(Utils.randomVector(nElements, Info.MIN_DATA_VALUE, Info.MAX_DATA_VALUE));
        setSortAlgorithm(sortAlgorithm);
    }

    /**
     *
     * @param listener
     * @return
     */
    public ArrayModel registerListener(DataChangedListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        listeners.add(listener);
        return this;
    }

    /**
     *
     * @return
     */
    public int[] getData() {
        return data;
    }

    /**
     *
     * @param data
     */
    public void setData(int[] data) {
        this.data = data;
        listeners.forEach(DataChangedListener::onDataChangedOrSet);
    }

    /**
     *
     * @param parameters
     */
    public void setSortAlgorithm(String parameters) {
        this.sortAlgorithm = AlgorithmFactory.create(parameters, data);
        listeners.forEach(DataChangedListener::onDataChangedOrSet);
    }

    /**
     *
     * @return
     */
    public AbstractAlgorithm getSortAlgorithm() {
        return sortAlgorithm;
    }

}
