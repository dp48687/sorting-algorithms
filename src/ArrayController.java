import algorithm.AbstractAlgorithm;

import java.util.HashSet;

/**
 *
 * @author danci
 * @version 2020.1
 */
public class ArrayController implements DataChangedListener {

    private Thread animationThread;

    private boolean animationRunning;

    private ArrayModel model;

    private ArrayView view;

    private int delay = Info.MIN_DATA_VALUE;

    private int nColumns = (Info.N_ELEMENTS_MIN + Info.N_ELEMENTS_MAX) / 2;

    private String algorithmID;

    /**
     *
     * @param model
     */
    public ArrayController(ArrayModel model) {
        this.model = model;
        model.registerListener(this);
        this.view = new ArrayView(this);
    }

    public void init() {
        if (!animationRunning) {
            model.init(nColumns, algorithmID);
        }
    }

    public synchronized void next() {
        model.getSortAlgorithm().nextStep();
        onDataChangedOrSet();
    }

    public void stop() {
        animationRunning = false;
        view.unfreezeControls();
    }

    public void play() {
        if (!animationRunning) {
            animationThread = new Thread(
                    () -> {
                        animationRunning = true;
                        view.freezeControls();
                        while (!model.getSortAlgorithm().hasFinished() && animationRunning) {
                            next();
                            try {
                                Thread.sleep(delay);
                            } catch (InterruptedException ignore) {

                            }
                        }
                        animationRunning = false;
                        view.unfreezeControls();
                    }
            );
            animationThread.setDaemon(true);
            animationThread.start();
        }
    }

    public ArrayView getView() {
        return view;
    }

    public void onAlgorithmChosen(String algorithmID) {
        this.algorithmID = algorithmID;
        if (!animationRunning) {
            if (model.getData() == null) {
                model.init(nColumns, algorithmID);
            } else {
                model.setSortAlgorithm(algorithmID);
            }
        }
    }

    public void onSpeedSet(double percentage) {
        int min = Info.ANIMATION_DELAY_MINIMUM;
        int max = Info.ANIMATION_DELAY_MAXIMUM;
        delay = (int) (min + (max - min) * (1.0 - percentage));
    }

    public void onNumberOfColumnsSet(int nColumns) {
        this.nColumns = nColumns;
    }

    @Override
    public void onDataChangedOrSet() {
        if (view != null) {
            AbstractAlgorithm algorithm = model.getSortAlgorithm();
            view.paintArray(
                    model.getData(),
                    algorithm == null ? new HashSet<>() : algorithm.getSorted(),
                    algorithm == null ? 0 : algorithm.nComparisons(),
                    algorithm == null ? 0 : algorithm.nReads(),
                    algorithm == null ? 0 : algorithm.nWrites()
            );
        }
    }

}
