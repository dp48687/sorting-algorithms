/**
 *
 * @author danci
 * @version 2020.1
 */
public interface DataChangedListener {

    /**
     * Called when data is set or in some other way changed.
     */
    void onDataChangedOrSet();

}
